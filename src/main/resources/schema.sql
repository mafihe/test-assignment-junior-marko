DROP TABLE IF EXISTS books;
DROP TABLE IF EXISTS book_ratings;

CREATE SEQUENCE rating_seq START WITH 1 INCREMENT BY 1;

CREATE TABLE IF NOT EXISTS books (
    isbn                  varchar(13) not null,
    title                 varchar(255) not null,
    author                varchar(255) not null,
    year_of_publication   int(10) not null,
    publisher             varchar(255),
    primary key (isbn)
);

CREATE TABLE IF NOT EXISTS book_ratings (
    id                  bigint        not null,
    isbn                varchar(13)   not null unique,
    book_rating         int(11)       not null,
    primary key  (id)
);

ALTER TABLE book_ratings
    ADD CONSTRAINT isbn_fkey
        FOREIGN KEY (isbn)
                REFERENCES books
                    ON DELETE CASCADE;