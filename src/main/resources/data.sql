INSERT INTO books VALUES ('0195153448','Classical Mythology','Mark P. O. Morford',2002,'Oxford University Press');
INSERT INTO books VALUES ('0002005018','Clara Callan','Richard Bruce Wright',2001,'HarperFlamingo Canada');

INSERT INTO books VALUES ('9780008226787','A Middle-Earth Traveller','John Howe',2018,'HarperCollins');
INSERT INTO books VALUES ('9788071457060','The Fellowship Of The Ring','J.R.R Tolkien',2005,'Allen & Unwin');
INSERT INTO books VALUES ('9780261102941','The Return Of The King','J.R.R Tolkien',2005,'Allen & Unwin');
INSERT INTO books VALUES ('9788372980618','The Two Towers','J.R.R Tolkien',2012,'Allen & Unwin');
INSERT INTO books VALUES ('9786047703739','Steve Jobs','Walter Isaacson',2011,'Simon & Schuster');
INSERT INTO books VALUES ('9780671043216','How to Win Friends and Influence People','Dale Carnegie',2011,'Simon & Schuster');
INSERT INTO books VALUES ('9780136083238','Clean Code','Robert Cecil Martin',2008,'Prentice Hall');
INSERT INTO books VALUES ('9788577800643','Patterns of Enterprise Application Architecture','Martin Fowler',2002,'Addison-Wesley Professional');

INSERT INTO book_ratings VALUES (rating_seq.nextval, '0195153448',0);
INSERT INTO book_ratings VALUES (rating_seq.nextval, '0002005018',10);

INSERT INTO book_ratings VALUES (rating_seq.nextval, '9780008226787',6);

INSERT INTO book_ratings VALUES (rating_seq.nextval, '9780261102941',6);
INSERT INTO book_ratings VALUES (rating_seq.nextval, '9788372980618',9);
INSERT INTO book_ratings VALUES (rating_seq.nextval, '9786047703739',8);
INSERT INTO book_ratings VALUES (rating_seq.nextval, '9780671043216',9);
INSERT INTO book_ratings VALUES (rating_seq.nextval, '9780136083238',7);
