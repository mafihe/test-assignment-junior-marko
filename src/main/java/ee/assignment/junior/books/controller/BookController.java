package ee.assignment.junior.books.controller;

import ee.assignment.junior.books.domain.Book;
import ee.assignment.junior.books.infra.BookRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
public class BookController {

    @Autowired
    BookRepository bookContext;

    @GetMapping("/books")
    public List<Book> getBooks(){

        return bookContext.findAll();
    }

    @RequestMapping("/book/{isbn}")
    public Optional<Book> getBook(@PathVariable("isbn") String isbn){

        return bookContext.findByIsbn(isbn);
    }

    @PostMapping(path="/book")
    public Book addBook(@RequestBody Book book){
        bookContext.save(book);
        return book;
    }

    @DeleteMapping("/book/{isbn}")
    public String deleteBook(@PathVariable String isbn){

        Book book = bookContext.getOne(isbn);
        bookContext.delete(book);

        return String.format("Book with isbn: %s is deleted", isbn);
    }

    @PutMapping(path="/book")
    public Book saveOrUpdateBook(@RequestBody Book book){
        bookContext.save(book);
        return book;
    }
}
