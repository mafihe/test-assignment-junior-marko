package ee.assignment.junior.books.controller;

import ee.assignment.junior.books.domain.Book;
import ee.assignment.junior.books.domain.Rating;
import ee.assignment.junior.books.infra.BookRepository;
import ee.assignment.junior.books.infra.RatingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
public class RatingController  {
    @Autowired
    BookRepository bookContext;
    @Autowired
    RatingRepository ratingContext;

    @GetMapping("/ratings")
    public List<Rating> getRatings(){
        return ratingContext.findAll();
    }

    @GetMapping("/rating/{id}")
    public Optional<Rating> getRating(@PathVariable Long id){
        return ratingContext.findById(id);
    }

    @DeleteMapping("/rating/{id}")
    public String deleteRating(@PathVariable Long id){

        Rating rating = ratingContext.getOne(id);
        ratingContext.delete(rating);

        return String.format("Rating with id: %s is deleted", id);
    }

    // Custom

    // REST endpoint to get all books with rating
    @GetMapping("/books/withRating")
    public List<Book> getBooksWithRating(){
        return bookContext.findAllBooksWithRating();
    }

    // REST endpoint to get book by ISBN with rating.
    @GetMapping("/book/{isbn}/withRating")
    public Book getBookWithRating(@PathVariable("isbn") String isbn){
        log.info("Starting searching book by rating");
        List<Book> booksWithRating = bookContext.findAllBooksWithRating();
        if (booksWithRating.isEmpty()){
            log.info("Books with ratings were not found");
        }
        Book book = null;

        for (Book b : booksWithRating){
            if (b.getIsbn().equals(isbn) ){
                book = b;
                log.info("ISBN match was found");
            }
        }
        if (book == null){
            log.info("Entity not found. Book has no rating or the book with given isbn doesn't exist");
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Entity not found. Book has no rating or the book with given isbn doesn't exist"
            );
        }
        return book;
    }

    // REST endpoint to add rating by book ISBN.
    @PutMapping(path="/book/{isbn}/addRating")
    public Rating addRating(@PathVariable String isbn, @RequestParam("rating") int ratingValue){
        Rating rating = null;

        if (ratingContext.existsByIsbn(isbn)){
            log.info("Rating already exist");
            rating = ratingContext.findByIsbn(isbn);
            rating.setRating(ratingValue);
        }
        else if (bookContext.existsById(isbn)){
            log.info("Book rating is missing, creating one");
            rating = new Rating();
            rating.setIsbn(isbn);
            rating.setRating(ratingValue);
        }
        else {
            log.info("Book by selected ISBN doesn't exist");
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Book by selected isbn doesn't exist"
            );
        }
        ratingContext.save(rating);
        return rating;
    }

    // REST endpoint to get ranking of books (top 10)
    @GetMapping("/books/top10")
    public List<Book> getTop10Books(){
        log.info("Getting top 10 books");
        return bookContext.findTop10Books();
    }

    // REST endpoint to get ranking of books (top 50)
    @GetMapping("/books/top50")
    public List<Book> getTop50Books(){
        log.info("Getting top 50 books");
        return bookContext.findTop50Books();
    }

    // REST endpoint to get ranking of books (top 100)
    @GetMapping("/books/top100")
    public List<Book> getTop100Books(){
        log.info("Getting top 100 books");
        return bookContext.findTop100Books();
    }
}
