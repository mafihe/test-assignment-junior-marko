package ee.assignment.junior.books.infra;

import ee.assignment.junior.books.domain.Book;
import ee.assignment.junior.books.domain.Rating;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RatingRepository extends JpaRepository<Rating, Long> {

    Rating findByIsbn(String isbn);
    Boolean existsByIsbn (String isbn);

}
