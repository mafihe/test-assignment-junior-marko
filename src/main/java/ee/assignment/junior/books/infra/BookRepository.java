package ee.assignment.junior.books.infra;

import ee.assignment.junior.books.domain.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface BookRepository extends JpaRepository<Book, String> {

    Optional<Book> findByIsbn(String isbn);

    String booksWithRatingsQuery = "SELECT * FROM books " +
            "INNER JOIN book_ratings ON books.isbn = book_ratings.isbn " +
            "WHERE book_ratings.isbn IS NOT null " +
            "ORDER BY book_ratings.book_rating DESC ";

    @Query(value = "SELECT * FROM books INNER JOIN book_ratings ON books.isbn = book_ratings.isbn WHERE book_ratings.isbn IS NOT null", nativeQuery = true)
    List<Book> findAllBooksWithRating();

    @Query(value = booksWithRatingsQuery +
            "LIMIT 10", nativeQuery = true)
    List<Book> findTop10Books();

    @Query(value = booksWithRatingsQuery +
            "LIMIT 50", nativeQuery = true)
    List<Book> findTop50Books();

    @Query(value = booksWithRatingsQuery +
            "LIMIT 100", nativeQuery = true)
    List<Book> findTop100Books();



}
