package ee.assignment.junior.books.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@Entity
@Table(name = "book_ratings")
public class Rating {

    @Id
    @SequenceGenerator(name = "rating_seq", sequenceName = "rating_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rating_seq")
    @Column(name = "id")
    private Long id;

    @Column(name = "isbn", nullable = false, unique = true)
    @NotNull
    @Size(max = 13, min = 1)
    private String isbn;

    @Column(name = "book_rating", nullable = false)
    @NotNull
    private int rating;

}
