package ee.assignment.junior.books.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "books")
@Getter
@Setter
public class Book {

    @Id
    @Column(name = "isbn")
    @NotNull
    @Size(max = 13, min = 1)
    private String isbn;

    @Column(name = "title", nullable = false)
    @NotNull
    @Size(max = 255, min = 1)
    private String title;

    @Column(name = "author", nullable = false)
    @NotNull
    @Size(max = 255, min = 1)
    private String author;

    @Column(name = "year_of_publication", nullable = false)
    @NotNull
    private int yearOfPublication;

    @Column(name = "publisher")
    @Size(max = 255)
    private String publisher;
}
